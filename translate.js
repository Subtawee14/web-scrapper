const { Translate } = require("@google-cloud/translate").v2;
const translate = new Translate({
  keyFilename: "api-key.json",
});

(async () => {
  const result = await translateText("สวัสดี", "en");
  console.log(result);
})();
async function translateText(text, target) {
  let [translations] = await translate.translate(text, target);
  translations = Array.isArray(translations) ? translations : [translations];
  return translations[0] ?? text;
}

async function detectLanguage(text) {
  let [detections] = await translate.detect(text);
  detections = Array.isArray(detections) ? detections : [detections];
  return detections[0].language ?? null;
}
module.exports = { translateText, detectLanguage };
