const puppeteer = require("puppeteer");
const fs = require("fs");
const list = require("./list");
const { translateText } = require("./translate");
const getTone = require("./tone_analyze");
const exportCsv = require("./export_csv");

/**
 * Main
 */
(async () => {
  let places = [];
  const browser = await puppeteer.launch({
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
    headless: true,
  });
  const page = await browser.newPage();
  for (const [urlIndex, url] of list.entries()) {
    await page.goto(url);
    /**
     * Get title name
     */
    await page.waitForSelector("._mbmcsn");
    const title = await page.$eval(
      "._e296pg ._mbmcsn > h1",
      (comment) => `${comment.innerText}`
    );

    /**
     * Get total records of comments
     */
    await page.waitForSelector("._1gjypya");
    const totalRecords = await page.$eval(
      "._9v5d4u > h2",
      (total) => `${total.innerText}`
    );

    /**
     * Declare variable
     */
    let place = {
      name: title,
      comments: [],
    };

    console.log("total records", +totalRecords.split(" ")[1].substring(1));

    /**
     * Scraping
     */
    const items = await scrapeInfiniteScrollItems(
      page,
      extractItems,
      +totalRecords.split(" ")[1].substring(1)
    );

    for (const [id, item] of items.entries()) {
      item.id = id;
      item.place = title;
    }

    console.log("start translate");
    for (const [index, item] of items.entries()) {
      console.log(
        `(${urlIndex + 1}/${list.length}) - ${title}: Translateing text: (${
          index + 1
        }/${items.length})`
      );
      item.translated = await translateText(item.comment, "en");
    }

    console.log("start tone analyze");
    for (const [index, item] of items.entries()) {
      console.log(
        `(${urlIndex + 1}/${list.length}) - ${title}: Analyze tone:  (${
          index + 1
        }/${items.length})`
      );
      item.tone = await getTone(item.translated);
    }
    place.comments = items;
    places.push(place);
  }

  await browser.close();
  let allComments = [];
  for (item of places) {
    allComments = [...allComments, ...item.comments];
  }

  await exportCsv(allComments);
  console.table(places);

  /**
   * Write file JSON
   */
  fs.writeFile("./result_JSON.json", JSON.stringify(places), "utf8", (err) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log("JSON File has been created");
  });
})();

/**
 * Get text
 */
async function extractItems() {
  const extractedElements = document.querySelectorAll("._1gjypya");
  console.log(extractedElements);
  const items = [];
  for (let [i, element] of extractedElements.entries()) {
    const string = element.innerText;
    const author = string.split("\n")[0];
    const date = string.split("\n")[1];
    const text = string.split("\n")[2];
    if (text) {
      items.push({ username: author, date, comment: text });
    }
  }
  return items;
}

/**
 * Scraper
 *
 * @param {*} page
 * @param {*} extractItems
 * @param {*} itemTargetCount
 */
async function scrapeInfiniteScrollItems(page, extractItems, itemTargetCount) {
  console.log("Start scraping");
  let scrollDelay = 1000;
  let items = [];
  try {
    let previousHeight;
    while (items.length < itemTargetCount) {
      
      await page.evaluate(() => {
        let elements = document.querySelectorAll("._1d784e5 > ._ejra3kg")
        for (let element of elements) element.click();
      });

      items = await page.evaluate(extractItems);

      console.log(items.length, itemTargetCount);

      previousHeight = await page.evaluate(
        'document.getElementsByClassName("_1v5ksyp")[0].scrollHeight'
      );

      await page.evaluate(
        'document.getElementsByClassName("_1v5ksyp")[0].scrollTo(0, document.getElementsByClassName("_1v5ksyp")[0].scrollHeight)'
      );

      currentHeight = await page.evaluate(
        'document.getElementsByClassName("_1v5ksyp")[0].scrollHeight'
      );

      await page.waitForFunction(
        `document.getElementsByClassName("_1v5ksyp")[0].scrollHeight > ${previousHeight}`
      );

      await page.waitFor(scrollDelay);
    }
  } catch (e) {
    console.error(e);
  }
  return items;
}
