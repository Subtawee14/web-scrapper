const axios = require("axios");

async function getTone(text) {
  const url =
    "https://api.jp-tok.tone-analyzer.watson.cloud.ibm.com/instances/70234b5e-a5df-4d3d-8ea9-ab6dbbfa1380/v3/tone";

  const params = {
    version: "2017-09-21",
    text,
  };

  const auth = {
    username: "apikey",
    password: "PpJZWeT7SNdxPUoxlKtXdrY4JGMLdQlRhaoeGDUhrWL_",
  };

  const result = await axios({
    method: "get",
    params,
    url,
    auth,
  });
  return result.data.document_tone.tones.length > 0
    ? result.data.document_tone.tones[0].tone_name
    : "No tone";
}

module.exports = getTone;