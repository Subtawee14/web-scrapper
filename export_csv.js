const { ExportToCsv } = require("export-to-csv");
const fs = require("fs");

async function exportCsv(data) {
  const options = {
    fieldSeparator: ",",
    quoteStrings: '"',
    decimalSeparator: ".",
    showLabels: true,
    showTitle: true,
    title: "Result",
    useTextFile: false,
    useBom: true,
    useKeysAsHeaders: true,
    // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
  };

  const csvExporter = new ExportToCsv(options);
  const csvData = csvExporter.generateCsv(data, true);
  fs.writeFileSync("data.csv", csvData);
}
module.exports = exportCsv;
